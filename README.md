# Defqon.1 2023 Live Broadcast

Data files regarding live broadcast, that took place on June 22-25 (jump to [Day 1](#day-1-thursday-june-22), [Day 2](#day-2-friday-june-23), [Day 3](#day-3-saturday-june-24), [Day 4](#day-4-sunday-june-25))

The project contains dirs with sfv, nfo & jpg files inside

## DAY 1 - Thursday, June 22

**BLUE**

- 18:00 Sub Zero Project
- 19:15 KELTEK - The Compass
- 19:45 Headhunterz
- 20:45 Sound Rush
- 21:30 Devin Wild - The_Innergame
- 22:00 Warface

**BLACK**

- 18:00 Outsiders & Korsakoff
- 19:00 Promo - Last Men Standing
- 20:00 Tha Playah
- 21:00 Frenchcore Familia (Dr. Peacock, BillX, The Sickest Squad)
- 22:00 Spitnoise

**INDIGO**

- 18:15 Cryex
- 18:45 Cryex vs Vexxed
- 19:15 Vexxed
- 19:45 Vasto
- 20:15 Vasto vs Oxya
- 20:45 Oxya
- 21:15 Kenai
- 21:45 Kenai vs Scarra
- 22:15 Scarra
- 22:45 Apex The Brotherhood

**MAGENTA**

- 18:15 Break of Dawn
- 19:15 ANDY SVGE
- 20:15 Toneshifterz
- 21:30 Ecstatic
- 22:45 Max Enforcer

**ENDSHOW**

- Defqon.1.2023.Day.1.Endshow.1080p.WEB.x264

## Video content

All video content of Day 1 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67770/defqon-1-2023-day-1-video-audio):


- Defqon.1.2023.Day.1.Blue.Sub.Zero.Project.1080p.WEB.x264
- Defqon.1.2023.Day.1.Blue.Keltek.1080p.WEB.x264
- Defqon.1.2023.Day.1.Blue.Headhunterz.1080p.WEB.x264
- Defqon.1.2023.Day.1.Blue.Sound.Rush.1080p.WEB.x264
- Defqon.1.2023.Day.1.Blue.Devin.Wild.1080p.WEB.x264
- Defqon.1.2023.Day.1.Blue.Warface.1080p.WEB.x264
- Defqon.1.2023.Day.1.Black.Outsiders.Korsakoff.1080p.WEB.x264
- Defqon.1.2023.Day.1.Black.Promo.1080p.WEB.x264
- Defqon.1.2023.Day.1.Black.Tha.Playah.1080p.WEB.x264
- Defqon.1.2023.Day.1.Black.Frenchcore.Familia.1080p.WEB.x264
- Defqon.1.2023.Day.1.Black.Spitnoise.1080p.WEB.x264


## Audio content

All audio content of Day 1 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67770/defqon-1-2023-day-1-video-audio):
- Devin_Wild_-_Defqon.1_2023_Day_1_Blue-WEB-2023
- Headhunterz_-_Defqon.1_2023_Day_1_Blue-WEB-2023
- Keltek_-_Defqon.1_2023_Day_1_Blue-WEB-2023
- Sound_Rush_-_Defqon.1_2023_Day_1_Blue-WEB-2023
- Sub_Zero_Project_-_Defqon.1_2023_Day_1_Blue-WEB-2023
- Warface_-_Defqon.1_2023_Day_1_Blue-WEB-2023
- Frenchcore_Familia_-_Defqon.1_2023_Day_1_Black-WEB-2023
- Outsiders_and_Korsakoff_-_Defqon.1_2023_Day_1_Black-WEB-2023
- Promo_-_Defqon.1_2023_Day_1_Black-WEB-2023
- Spitnoise_-_Defqon.1_2023_Day_1_Black-WEB-2023
- Tha_Playah_-_Defqon.1_2023_Day_1_Black-WEB-2023
- VA_-_Defqon.1_2023_Day_1_Indigo-WEB-2023
- VA_-_Defqon.1_2023_Day_1_Magenta-WEB-2023

***

## DAY 2 - Friday, June 23

**RED**

- 13:00 The Opening Ceremony with Phuture Noize
- 14:00 Darren Styles
- 15:00 Atmozfears - The Reawakening
- 15:45 TNT
- 16:45 Outsiders
- 17:30 Hard Driver
- 18:15 Da Tweekaz
- 19:15 D-Block & S-te-Fan
- 20:15 B-Front
- 21:00 Act of Rage
- 21:45 The Gang Live
- 22:20 The Spotlight with Rebelion

**BLUE**

- 11:00 RVAGE
- 12:30 Clockartz
- 13:30 Scarra & Vasto present Bassline Breaker
- 14:00 Invector presents Electronic Exuberance
- 14:30 Code Black
- 15:15 Unresolved
- 16:00 Alter Ego
- 16:45 Rejecta & Cryex
- 17:45 Killshot
- 18:30 The Purge & Adjuzt
- 19:30 Mish
- 20:30 Imperatorz & Neroz
- 21:30 Mutilator & Anderex present Neon Future Live

**BLACK**

- 11:00 Hardcore Confessions with Audiofreq
- 12:00 Unfused
- 13:00 Broken Minds
- 14:00 Re-Style
- 15:00 Nosferatu
- 16:00 Evil Activities
- 17:00 Partyraiser
- 18:15 Bulletproof & Lady Dammage & Juliëx
- 19:45 Major Conspiracy
- 20:45 Barber
- 21:45 Dither
- 22:30 The Spotlight with Angerfist

**UV**

- 11:00 Envine
- 12:00 Retrospect
- 12:45 Hardstyle Pianist
- 13:15 Gammer
- 14:00 Lady Faith
- 14:45 Xception
- 15:15 Audiotricz & Ecstatic present Progressive Hardstyle
- 15:45 Bass Modulators
- 16:30 The Pitcher Live
- 17:15 2▲1 (Sound Rush & Atmozfears)
- 18:00 B-Frontliner
- 18:45 Primeshock presents Shockwork
- 19:30 JDX Live
- 20:15 3 Blokes (Code Black, Toneshifterz, Audiofreq)
- 21:00 Billx presents The Rave Music Live Show
- 21:45 "D:/CHILDHOOD/TRAUMA/GPF/LIVE/THEPIEPSHOW2023.WAV"

**YELLOW**

- 11:00 Neika
- 12:00 Juju Rush
- 13:00 Relianze
- 14:00 Le Bask
- 15:00 The Satan
- 16:00 Deathroar
- 17:00 Unproven
- 18:00 Trespassed
- 19:00 Lunakorpz
- 19:45 Cryogenic
- 20:30 TerrorClown
- 21:15 Drokz

**INDIGO**

- 11:00 K1
- 12:00 Sins Of Insanity
- 13:00 Amentis
- 14:00 Level One
- 15:00 Luner
- 16:00 Zyon
- 17:00 Delius
- 18:00 Radianze
- 19:00 Bright Visions
- 20:00 Oxya
- 21:00 Vyral

**MAGENTA**

- 11:00 Prefix & Density
- 12:00 Artifact
- 13:00 Degos & Re-Done
- 14:00 Jones
- 15:00 Crypsis & Chain Reaction
- 16:00 Digital Punk
- 17:00 Sub Sonik
- 18:00 Jason Payne ''Goldschool''
- 19:00 Regain
- 19:45 E-Force
- 20:30 Chris One
- 21:15 Main Concern
- 23:00 REVIVE
- 23:30 Primeshock
- 00:00 The Darkraver & Hans Glock
- 00:30 Blame Noise

**GOLD**

- 11:00 T-Go
- 12:30 Franky Jones
- 13:30 Dano
- 14:30 Tommyknocker
- 15:30 Panic
- 17:00 Endymion
- 18:00 Korsakoff
- 19:00 Promo
- 20:00 Ruffneck
- 21:00 The Sickest Squad

**SILVER**

- 23:00 STOIK + bitface
- 23:30 Geck-O + bitface
- 00:00 Geck-O + D00d
- 00:30 DJ Hidden + D00d
- 01:00 DJ Hidden + Geck-o
- 01:30 Koarse + Geck-o
- 02:00 Koarse + D++d
- 02:30 The Mandi Experience

**GREEN**

- 11:00 Geck-O
- 12:00 DAE
- 13:00 10CLS
- 14:30 ALT8
- 16:00 Tham
- 17:00 Farrago
- 18:00 APHOTHIC
- 19:00 Brecc
- 20:00 Caravel
- 21:00 Ghost In The Machine
- 22:00 Somniac One

**ENDSHOW**

- Defqon.1.2023.Day.2.Endshow.1080p.WEB.x264

## Video content

All video content of Day 2 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67817/defqon-1-2023-day-2-video-audio):

- Defqon.1.2023.Day.2.Black.Angerfist.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Audiofreq.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Barber.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Broken.Minds.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Bulletproof.Lady.Dammage.Juliex.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Dither.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Evil.Activities.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Major.Conspiracy.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Nosferatu.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Partyraiser.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Re-Style.1080p.WEB.x264
- Defqon.1.2023.Day.2.Black.Unfused.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Alter.Ego.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Clockartz.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Code.Black.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Imperatorz.Neroz.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Invector.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Killshot.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Mish.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Mutilator.Anderex.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Rejecta.Cryex.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Rvage.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Scarra.&.Vasto.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.The.Purge.Adjuzt.1080p.WEB.x264
- Defqon.1.2023.Day.2.Blue.Unresolved.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Act.Of.Rage.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Atmozfears.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.B-Front.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Da.Tweekaz.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Darren.Styles.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.D-Block.S-te-Fan.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Hard.Driver.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Outsiders.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Phuture.Noize.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.Rebelion.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.The.Gang.Live.1080p.WEB.x264
- Defqon.1.2023.Day.2.Red.TNT.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Audiotricz.Ecstatic.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Bass.Modulators.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.B-Frontliner.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Billx.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Code.Black.Toneshifterz.Audiofreq.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Envine.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Gammer.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Hardstyle.Pianist.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.JDX.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Lady.Faith.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Primeshock.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Retrospect.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Sound.Rush.Atmozfears.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.The.Pitcher.1080p.WEB.x264
- Defqon.1.2023.Day.2.UV.Xception.1080p.WEB.x264


## Audio content

All audio content of Day 2 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67817/defqon-1-2023-day-2-video-audio):

VA_-_Defqon.1_2023_Day_2_Black
- Angerfist_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Audiofreq_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Barber_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Broken_Minds_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Bulletproof_and_Lady_Dammage_and_Juliex_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Dither_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Evil_Activities_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Major_Conspiracy_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Nosferatu_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Partyraiser_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Re-Style_-_Defqon.1_2023_Day_2_Black-WEB-2023
- Unfused_-_Defqon.1_2023_Day_2_Black-WEB-2023

VA_-_Defqon.1_2023_Day_2_Blue
- Alter_Ego_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Clockartz_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Code_Black_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Imperatorz_and_Neroz_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Invector_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Killshot_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Mish_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Mutilator_and_Anderex_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Rejecta_and_Cryex_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- RVAGE_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Scarra_and_Vasto_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- The_Purge_and_Adjuzt_-_Defqon.1_2023_Day_2_Blue-WEB-2023
- Unresolved_-_Defqon.1_2023_Day_2_Blue-WEB-2023

VA_-_Defqon.1_2023_Day_2_Red
- Act_Of_Rage_-_Defqon.1_2023_Day_2_Red-WEB-2023
- Atmozfears_-_Defqon.1_2023_Day_2_Red-WEB-2023
- B-Front_-_Defqon.1_2023_Day_2_Red-WEB-2023
- Darren_Styles_-_Defqon.1_2023_Day_2_Red-WEB-2023
- Da_Tweekaz_-_Defqon.1_2023_Day_2_Red-WEB-2023
- D-Block_and_S-te-Fan_-_Defqon.1_2023_Day_2_Red-WEB-2023
- Hard_Driver_-_Defqon.1_2023_Day_2_Red-WEB-2023
- Outsiders_-_Defqon.1_2023_Day_2_Red-WEB-2023
- Phuture_Noize_-_Defqon.1_2023_Day_2_Red-WEB-2023
- Rebelion_-_Defqon.1_2023_Day_2_Red-WEB-2023
- The_Gang_Live_-_Defqon.1_2023_Day_2_Red-WEB-2023
- TNT_-_Defqon.1_2023_Day_2_Red-WEB-2023

VA_-_Defqon.1_2023_Day_2_UV
- Audiotricz_and_Ecstatic_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Bass_Modulators_-_Defqon.1_2023_Day_2_UV-WEB-2023
- B-Frontliner_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Billx_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Code_Black_and_Toneshifterz_and_Audiofreq_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Envine_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Gammer_-_Defqon.1_2023_Day_2_UV-WEB-2023
- GPF_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Hardstyle_Pianist_-_Defqon.1_2023_Day_2_UV-WEB-2023
- JDX_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Lady_Faith_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Primeshock_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Retrospect_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Sound_Rush_and_Atmozfears_-_Defqon.1_2023_Day_2_UV-WEB-2023
- The_Pitcher_-_Defqon.1_2023_Day_2_UV-WEB-2023
- Xception_-_Defqon.1_2023_Day_2_UV-WEB-2023

.
- VA_-_Defqon.1_2023_Day_2_Silver-WEB-2023
- VA_-_Defqon.1_2023_Day_2_Gold-WEB-2023
- VA_-_Defqon.1_2023_Day_2_Green-WEB-2023
- VA_-_Defqon.1_2023_Day_2_Yellow-WEB-2023
- VA_-_Defqon.1_2023_Day_2_Indigo-WEB-2023
- VA_-_Defqon.1_2023_Day_2_Magenta-WEB-2023
- VA_-_Defqon.1_2023_Day_2_Endshow-WEB-2023


## DAY 3 - Saturday, June 24

**RED**

- 12:00 Galactixx
- 13:00 Audiotricz & Sephyx
- 14:00 Frontliner
- 15:00 Sound Rush #Destination
- 16:00 POWER HOUR
- 17:00 Daybreak Session with Geck-o
- 17:30 Brennan Heart & Wildstylez
- 19:00 Ran-D
- 20:00 Vertile
- 20:45 Sub Zero Project & Devin Wild
- 21:45 D-Sturb
- 22:35 The Endshow

**BLUE**

- 11:00 Aversion
- 12:15 Endymion
- 13:30 Bloodlust
- 14:30 Bass X Machina
- 15:00 Regain
- 16:15 Riot Shift
- 17:15 Adaro & Digital Punk
- 18:30 The Purge presents Trippin'
- 19:15 Cryex
- 20:15 Rooler
- 21:15 Malice

**BLACK**

- 11:00 Art of Fighters
- 12:00 The Viper & Karun
- 13:00 Ruffneck 30 years of Hardcore
- 14:00 Gridkiller
- 15:00 Never Surrender
- 16:00 Floxytek
- 17:30 AniMe
- 18:30 Restrained
- 19:30 D-Fence Live
- 20:00 Hysta
- 21:00 F.Noize & DRS
- 22:00 Dimitri K

**UV**

- 11:00 Warrior Workout
- 11:45 Crude Intentions
- 12:30 MANDY
- 13:15 DJ Isaac
- 14:15 Demi Kanon
- 15:00 Jay Reeve & Refuzion
- 16:00 Avi8
- 17:00 Stormerz
- 18:15 Coone presents A New Decade
- 19:15 Adrenalize presents Toxic
- 19:45 Psyko Punkz
- 20:45 Zatox presents Italian Hardstyle
- 21:30 Frequencerz

**YELLOW**

- 11:00 Doris
- 12:00 Røza
- 12:45 JKLL
- 13:45 D-Frek
- 14:45 Sprinky
- 15:45 Yoshiko
- 16:30 Preceptor Live
- 17:00 Irradiate
- 17:45 Vandal!sm
- 18:30 Soulblast
- 19:15 Imperial - Mayhem LIVE
- 19:45 Levenkhan
- 20:30 Offensive Rage LIVE: Cryogenic vs. Soulblast vs. Abaddon
- 21:15 MBK

**INDIGO**

- 11:00 Vexxed
- 12:00 D-Attack presents Project Attack
- 13:00 Luminite
- 14:00 The Savage Squad: Adjuzt, Griever, Omnya, PL4Y, The Purge
- 15:00 Physika
- 16:00 Chapter V
- 17:00 Krowdexx
- 18:00 Mutilator
- 19:00 Omnya
- 20:00 Scarra
- 21:00 Fraw
- 22:00 Kruelty & Element

**MAGENTA**

- 11:00 Sunny D
- 12:30 Balistic
- 14:00 DJ Thera & Geck-o
- 15:00 Vicente One More Time
- 16:00 Davide Sonar
- 17:00 Pavo
- 18:00 Tatanka
- 19:00 Deepack & Luna
- 20:30 Donkey Rollers LIVE
- 21:00 Alpha Twins
- 22:00 
- 23:00 Geck-o
- 23:30 Creeds
- 00:15 MØSCARDO & XRTN

**GOLD**

- 11:00 Artcore with Ruffneck
- 12:00 Dazzler
- 13:00 Francois
- 14:00 Sequence & Ominous
- 15:00 The Darkraver
- 16:00 Marc Acardipane
- 17:00 J.D.A.
- 18:00 The Viper
- 19:30 Art of Fighters
- 20:30 Partyraiser
- 21:30 Unexist & DaY-Már

**SILVER**

- 11:00 Speedcore Dating with Akira
- 12:00 Rude Awakening
- 13:30 Sacerdos Vigilia
- 15:00 Nanostorm LIVE
- 16:30 The DJ Producer
- 17:30 The Silence
- 18:30 DNGN DRGNS
- 19:30 Igneon System
- 21:00 Fish & Rice

**PURPLE**

- 12:00 Exodus
- 12:45 Maria Paz
- 13:30 RWND
- 14:15 Stephen Game
- 15:00 State Of Deva
- 15:45 TCM
- 16:30 A-RIZE
- 17:15 Maxtreme
- 18:00 Sanctuary
- 18:45 Kenai
- 19:30 Rogue Zero
- 20:15 Phantom
- 21:00 Aranxa
- 21:45 Spiady

## Video content

All video content of Day 3 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67872/defqon-1-2023-day-3-video-audio):

- Defqon.1.2023.Day.3.Black.Anime.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Art.Of.Fighters.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.D-Fence.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Dimitri.K.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.F.Noize.DRS.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Floxytek.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Gridkiller.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Hysta.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Never.Surrender.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Restrained.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.Ruffneck.1080p.WEB.x264
- Defqon.1.2023.Day.3.Black.The.Viper.Karun.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Adaro.Digital.Punk.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Aversion.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Bass.X.Machina.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Bloodlust.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Cryex.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Endymion.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Malice.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Regain.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Riot.Shift.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.Rooler.1080p.WEB.x264
- Defqon.1.2023.Day.3.Blue.The.Purge.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Audiotricz.Sephyx.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Brennan.Heart.Wildstylez.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.D-Sturb.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Frontliner.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Galactixx.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Geck-O.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Power.Hour.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Ran-D.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Sound.Rush.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Sub.Zero.Project.Devin.Wild.1080p.WEB.x264
- Defqon.1.2023.Day.3.Red.Vertile.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Adrenalize.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Avi8.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Coone.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Crude.Intentions.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Demi.Kanon.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.DJ.Isaac.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Frequencerz.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Jay.Reeve.Refuzion.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Mandy.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Psyko.Punkz.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Stormerz.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Warrior.Workout.1080p.WEB.x264
- Defqon.1.2023.Day.3.UV.Zatox.1080p.WEB.x264

## Audio content

All audio content of Day 3 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67872/defqon-1-2023-day-3-video-audio):

VA_-_Defqon.1_2023_Day_3_Black
- AniMe_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Art_Of_Fighters_-_Defqon.1_2023_Day_3_Black-WEB-2023
- D-Fence_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Dimitri_K_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Floxytek_-_Defqon.1_2023_Day_3_Black-WEB-2023
- F.Noize_and_DRS_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Gridkiller_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Hysta_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Never_Surrender_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Restrained_-_Defqon.1_2023_Day_3_Black-WEB-2023
- Ruffneck_-_Defqon.1_2023_Day_3_Black-WEB-2023
- The_Viper_and_Karun_-_Defqon.1_2023_Day_3_Black-WEB-2023

VA_-_Defqon.1_2023_Day_3_Blue
- Adaro_and_Digital_Punk_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Aversion_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Bass_X_Machina_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Bloodlust_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Cryex_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Endymion_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Malice_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Regain_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Riot_Shift_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- Rooler_-_Defqon.1_2023_Day_3_Blue-WEB-2023
- The_Purge_-_Defqon.1_2023_Day_3_Blue-WEB-2023

VA_-_Defqon.1_2023_Day_3_Red
- Audiotricz_and_Sephyx_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Brennan_Heart_and_Wildstylez_-_Defqon.1_2023_Day_3_Red-WEB-2023
- D-Sturb_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Frontliner_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Galactixx_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Geck-o_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Power_Hour_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Ran-D_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Sound_Rush_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Sub_Zero_Project_and_Devin_Wild_-_Defqon.1_2023_Day_3_Red-WEB-2023
- Vertile_-_Defqon.1_2023_Day_3_Red-WEB-2023

VA_-_Defqon.1_2023_Day_3_UV
- Adrenalize_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Avi8_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Coone_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Crude_Intentions_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Demi_Kanon_-_Defqon.1_2023_Day_3_UV-WEB-2023
- DJ_Isaac_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Frequencerz_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Jay_Reeve_and_Refuzion_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Mandy_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Psyko_Punkz_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Stormerz_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Warrior_Workout_-_Defqon.1_2023_Day_3_UV-WEB-2023
- Zatox_-_Defqon.1_2023_Day_3_UV-WEB-2023

.
- VA_-_Defqon.1_2023_Day_3_Gold-WEB-2023
- VA_-_Defqon.1_2023_Day_3_Indigo-WEB-2023
- VA_-_Defqon.1_2023_Day_3_Magenta-WEB-2023
- VA_-_Defqon.1_2023_Day_3_Purple-WEB-2023
- VA_-_Defqon.1_2023_Day_3_Silver-WEB-2023
- VA_-_Defqon.1_2023_Day_3_Yellow-WEB-2023
- VA_-_Defqon.1_2023_Day_3_Endshow-WEB-2023


## DAY 4 - Sunday, June 25

**RED**

- 20:00 Defqon.1 Legends
- 22:00 DJ The Prophet - From the Hard "The Final"
- 22:50 The Closing Ritual

**BLUE**

- 12:00 Luna
- 12:45 Deluzion
- 13:45 Aftershock
- 14:45 Degos & Re-done
- 15:45 Kronos presents Kryptonite
- 16:15 Voidax
- 17:15 Gunz For Hire - Baddest on the Block
- 18:00 Rejecta
- 18:45 Vertile
- 19:45 Sub Sonik
- 21:00 E-Force
- 22:00 Sickmode

**BLACK**

- 11:00 Open Airbed Concert with JDX
- 12:00 Fantastic Four: Panic, Vince, The Viper, The Darkraver
- 14:00 Crypton
- 15:00 Mad Dog
- 16:00 N-Vitral
- 17:00 Miss K8
- 18:00 Deadly Guns
- 19:00 Dr. Peacock

**UV**

- 11:00 DJ Jean
- 12:00 Bassbrain
- 12:45 De Kraaien - Raaf Set!
- 13:15 Ransom
- 14:00 STUK
- 14:30 Bass Chaserz
- 15:30 Altijd Larstig & Rob Gasd'rop
- 16:30 FeestDJRuthless
- 17:30 De Nachtbrakers (Endymion, Degos & Re-Done, Bass Chaserz)
- 18:30 Outsiders & Pat B
- 19:30 Mark with a K & MC Chucky
- 20:30 Dr. Rude
- 21:15 JeBroer 4 Ever Experience
- 22:00 Paul Elstak

**YELLOW**

- 11:00 Jur Terreur
- 12:00 Noxiouz
- 13:00 Mr. Ivex
- 14:00 Lil Texas
- 15:00 Hard Effectz
- 16:00 The Dope Doctor
- 17:00 Chaotic Hostility
- 18:00 Angernoizer
- 18:45 Spitnoise
- 19:45 System Overload
- 20:30 Noisekick
- 21:15 HKV Live

**INDIGO**

- 12:30 So Juice
- 13:15 Apexx
- 14:00 The Straikerz presents Jackpot.1 LIVE
- 14:30 Vasto
- 15:30 Anderex
- 16:30 Neroz - Album Showcase
- 17:00 Adjuzt
- 18:00 Unresolved
- 19:00 Spoontechnicians

**MAGENTA**

- 11:00 Omegatypez
- 12:00 Lip DJ presents World Of Hardstyle
- 13:00 Waverider
- 14:00 Wasted Penguinz
- 15:00 The Pitcher
- 16:00 Villain & DV8 present The Legacy
- 16:45 Atmozfears & Adrenalize
- 17:45 Psyko Punkz
- 18:30 Frequencerz Live
- 19:00 Zany "25 Years"

**SUNDAY**

- 11:00 DJ Rob & MC Joe
- 12:30 Critical Mass
- 14:00 Catscan
- 15:00 Vince
- 16:00 Bass-D
- 17:00 Evil Activities
- 18:00 Tha Playah
- 19:00 Ophidian & The Outside Agency

**SILVER**

- 11:00 Doormouse
- 11:45 Rabbeat
- 12:30 Embrionyc
- 13:30 [KRTM]
- 14:30 Iridium
- 15:30 Kilbourne
- 17:00 Manu Le Malin
- 18:00 Mad Dog Downtempo set
- 19:00 I:GOR

**PURPLE**

- 11:00 Solstice
- 11:45 Releazer
- 12:30 BENGR
- 13:15 Cango
- 14:00 DEEZL
- 14:45 PL4Y
- 15:30 Shockrage
- 16:15 Phyric
- 17:00 Posyden
- 17:45 Exproz
- 18:30 Griever
- 19:15 Tanukichi


## Video content

All video content of Day 4 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67916/defqon-1-2023-day-4-video-audio):

- Defqon.1.2023.Day.4.Black.Crypton.1080p.WEB.x264
- Defqon.1.2023.Day.4.Black.Deadly.Guns.1080p.WEB.x264
- Defqon.1.2023.Day.4.Black.Dr.Peacock.1080p.WEB.x264
- Defqon.1.2023.Day.4.Black.Fantastic.Four.1080p.WEB.x264
- Defqon.1.2023.Day.4.Black.JDX.1080p.WEB.x264
- Defqon.1.2023.Day.4.Black.Mad.Dog.1080p.WEB.x264
- Defqon.1.2023.Day.4.Black.Miss.K8.1080p.WEB.x264
- Defqon.1.2023.Day.4.Black.N-Vitral.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Aftershock.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Degos.Re-Done.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Deluzion.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.E-Force.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Gunz.For.Hire.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Kronos.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Luna.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Rejecta.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Sickmode.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Sub.Sonik.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Vertile.1080p.WEB.x264
- Defqon.1.2023.Day.4.Blue.Voidax.1080p.WEB.x264
- Defqon.1.2023.Day.4.Red.Legends.1080p.WEB.x264
- Defqon.1.2023.Day.4.Red.The.Prophet.1080p.WEB.x264
- Defqon.1.2023.Day.4.The.Closing.Ritual.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Altijd.Larstig.Rob.Gasdrop.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Bass.Chaserz.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Bassbrain.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.De.Kraaien.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.De.Nachtbrakers.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.DJ.Jean.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Dr.Rude.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Feest.DJRuthless.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Jebroer.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Mark.With.AK.MC.Chucky.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Outsiders.Pat.B.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Paul.Elstak.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Ransom.1080p.WEB.x264
- Defqon.1.2023.Day.4.UV.Stuk.1080p.WEB.x264

## Audio content

All audio content of Day 4 can be downloaded from [1gabba.pw](https://1gabba.pw/node/67916/defqon-1-2023-day-4-video-audio):

VA_-_Defqon.1_2023_Day_4_Black
- Crypton_-_Defqon.1_2023_Day_4_Black-WEB-2023
- Deadly_Guns_-_Defqon.1_2023_Day_4_Black-WEB-2023
- Dr_Peacock_-_Defqon.1_2023_Day_4_Black-WEB-2023
- Fantastic_Four_-_Defqon.1_2023_Day_4_Black-WEB-2023
- JDX_-_Defqon.1_2023_Day_4_Black-WEB-2023
- Mad_Dog_-_Defqon.1_2023_Day_4_Black-WEB-2023
- Miss_K8_-_Defqon.1_2023_Day_4_Black-WEB-2023
- N-Vitral_-_Defqon.1_2023_Day_4_Black-WEB-2023

VA_-_Defqon.1_2023_Day_4_Blue
- Aftershock_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Degos_and_Re-Done_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Deluzion_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- E-Force_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Gunz_For_Hire_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Kronos_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Luna_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Rejecta_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Sickmode_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Sub_Sonik_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Vertile_-_Defqon.1_2023_Day_4_Blue-WEB-2023
- Voidax_-_Defqon.1_2023_Day_4_Blue-WEB-2023

VA_-_Defqon.1_2023_Day_4_Red
- The_Closing_Ritual_-_Defqon.1_2023_Day_4_Red-WEB-2023
- The_Prophet_-_Defqon.1_2023_Day_4_Red-WEB-2023
- VA_-_Defqon.1_Legends_Day_4_Red-WEB-2023

VA_-_Defqon.1_2023_Day_4_UV
- Altijd_Larstig_and_Rob_Gasdrop_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Bassbrain_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Bass_Chaserz_-_Defqon.1_2023_Day_4_UV-WEB-2023
- De_Kraaien_-_Defqon.1_2023_Day_4_UV-WEB-2023
- De_Nachtbrakers_-_Defqon.1_2023_Day_4_UV-WEB-2023
- DJ_Jean_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Dr_Rude_-_Defqon.1_2023_Day_4_UV-WEB-2023
- FeestDJRuthless_-_Defqon.1_2023_Day_4_UV-WEB-2023
- JeBroer_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Mark_with_AK_and_MC_Chucky_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Outsiders_and_Pat_B_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Paul_Elstak_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Ransom_-_Defqon.1_2023_Day_4_UV-WEB-2023
- Stuk_-_Defqon.1_2023_Day_4_UV-WEB-2023

.
- VA_-_Defqon.1_2023_Day_4_Gold-WEB-2023
- VA_-_Defqon.1_2023_Day_4_Indigo-WEB-2023
- VA_-_Defqon.1_2023_Day_4_Magenta-WEB-2023
- VA_-_Defqon.1_2023_Day_4_Purple-WEB-2023
- VA_-_Defqon.1_2023_Day_4_Silver-WEB-2023
- VA_-_Defqon.1_2023_Day_4_Yellow-WEB-2023


***
